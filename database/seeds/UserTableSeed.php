<?php

use Illuminate\Database\Seeder;
use App\User;
use Caffeinated\Shinobi\Models\Role;


class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
        	'name'		=> 'Administrator',
        	'slug'  	=> 'adm',
        	'special' 	=> 'all-access'
        ]);

        Role::create([
        	'name'		=> 'Supervisor',
        	'slug'  	=> 'superv',
        	'special' 	=> 'all-access'
        ]);

        Role::create([
        	'name'		=> 'Adviser',
        	'slug'  	=> 'adv',
        	'special' 	=> 'all-access'
        ]);

        Role::create([
        	'name'		=> 'Operator',
        	'slug'  	=> 'ope',
        	'special' 	=> 'all-access'
        ]);


        User::create(
            [
                'first_name'=> 'Administrador',
                'last_name' => 'adm',
                'email' => 'administrador@amg.com',
                'phone'=> '+519999999',
                'cellphone' => '00000000',
                'address' => 'San Juan de Luringancho, Lima, Peru',
                'date_of_birth' => date('1984/12/23'),
                'password' => bcrypt('123456'),
                'status'=> 1,
                'restriction_by_ip' => 0
            ]
        );

        User::create(
            [
                'first_name'=> 'Test',
                'last_name' => 'user',
                'email' => 'test@amg.com',
                'phone'=> '+519999999',
                'cellphone' => '00000000',
                'address' => 'San Juan de Luringancho, Lima, Peru',
                'date_of_birth' => date('1984/12/23'),
                'password' => bcrypt('123456'),
                'status'=> 1,
                'restriction_by_ip' => 0
            ]
        );
    }
}
