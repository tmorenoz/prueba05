<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;


class PermissionsTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          //general
          Permission::create([
            'name'          => 'Views data',
            'slug'          => 'data.view',
            'description'   => 'Lista y navega todos los usuarios del sistema',
        ]);
        Permission::create([
            'name'          => 'Create data',
            'slug'          => 'data.create',
            'description'   => 'Ve en detalle cada usuario del sistema',            
        ]);
        
        Permission::create([
            'name'          => 'Edit data',
            'slug'          => 'data.edit',
            'description'   => 'Podría editar cualquier dato de un usuario del sistema',
        ]);
        
        Permission::create([
            'name'          => 'Delete data',
            'slug'          => 'data.delete',
            'description'   => 'Podría eliminar cualquier usuario del sistema',      
        ]);
        
       
        

    }
}
