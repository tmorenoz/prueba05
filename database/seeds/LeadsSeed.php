<?php

use Illuminate\Database\Seeder;
use App\Leadsource;

use App\Leadstatus;
use App\Program;

class LeadsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // load source
        Leadsource::create([
            'value' => 'Advertisement'
        ]);
        Leadsource::create([
            'value' => 'Employee Referral'
        ]);
        Leadsource::create([
            'value' => 'External Referral'
        ]);

        Leadsource::create([
            'value' => 'Client Referral'
        ]);
        Leadsource::create([
            'value' => 'Partner'
        ]);
        Leadsource::create([
            'value' => 'Public Relationship'
        ]);
        Leadsource::create([
            'value' => 'Chat'
        ]);

        // load data lead source name

      /*  Leadsourcename::create([
                'value' => 'WWCE Employee'
        ]);
        Leadsourcename::create([
                'value'=> 'WWCE Client'
        ]);
        Leadsourcename::create([
                'value'=> '100.3 amor'
        ]);
        Leadsourcename::create([
            'value'=> 'Que buena 98.9 FM'
        ]);
        Leadsourcename::create([
            'value'=> 'La Favorita 95.9FM'
        ]);
        Leadsourcename::create([
            'value'=> 'Tricolor Sacramento 99.9 FM'
        ]);
        Leadsourcename::create([
            'value'=> 'La Raza 93.3'
        ]);
        Leadsourcename::create([
            'value'=> 'La Ranchera 96.7'
        ]);
        Leadsourcename::create([
            'value'=> 'La Kaliente 1370AM'
        ]);
        Leadsourcename::create([
            'value'=> 'Lazer 93.9 Modesto'
        ]);
        Leadsourcename::create([
            'value'=> 'Lazer 93.7 San Jose'
        ]);
        Leadsourcename::create([
            'value'=> 'Lazer 94.3  Sacramento'
        ]);
        Leadsourcename::create([
            'value'=> 'La Bamba - Revista'
        ]);
        Leadsourcename::create([
            'value'=> 'Patricia Castro'
        ]);
        Leadsourcename::create([
            'value'=> 'LA KIQI 1010AM'
        ]);
        Leadsourcename::create([
            'value'=> 'Que Buena LA'
        ]);
        Leadsourcename::create([
            'value'=> 'Que Buena LA'
        ]);*/

        // program
        Program::create([
            'value' => 'Business'
        ]);
        Program::create([
            'value' => 'Boost Credit'
        ]);
        Program::create([
            'value' => 'Credit Repair'
        ]);
        Program::create([
            'value' => 'Debt Solution'
        ]);
        Program::create([
            'value' => 'Tax Research'
        ]);



        Leadstatus::create([
            'value' => 'Attempted to Contact'
        ]);
        Leadstatus::create([
            'value' => 'Contact in Future'
        ]);
        Leadstatus::create([
            'value' => 'Contacted'
        ]);
        Leadstatus::create([
            'value' => 'Lost Lead'
        ]);
        Leadstatus::create([
            'value' => 'Not Contacted'
        ]);
        Leadstatus::create([
            'value' => 'Pending'
        ]);
        Leadstatus::create([
            'value' => 'Client'
        ]);
        Leadstatus::create([
            'value' => 'Inactive'
        ]);
        Leadstatus::create([
            'value' => 'Junk Lead'
        ]);
        Leadstatus::create([
            'value' => 'Not Qualified'
        ]);
    }
}
