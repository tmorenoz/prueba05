<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <title>{{ config('app.name', 'Laravel') }}</title>
     <!-- Scripts -->
     <script src="{{ asset('js/app.js') }}" defer></script>
     <!-- Styles -->
     <link href="{{ asset('css/app.css') }}" rel="stylesheet">

     <style>
                .loginbody 
                        {
                                background-image: url('../images/background.jpg');
                             
                                background-position: center; /* Center the image */
                                background-repeat: no-repeat; /* Do not repeat the image */
                                background-size: cover; /* Resize the background image to cover the entire container */;
                        }

                       

                        a:link {
                        text-decoration: none;
                        color: white;
                        }

                        a:visited {
                        text-decoration: none;
                        color: white;
                        }

                        a:hover {
                        text-decoration: none;
                        color: white;
                        }

                        a:active {
                        text-decoration: none;
                        color: white;
                        }
         </style>
  </head>
  <body class="hold-transition login-page loginbody ">
                        <div class="row  mt-2">
                                <div class="col-md-12">
                                                @if (session('status'))
                                                        <div class="alert alert-success">
                                                                {{ session('status') }}
                                                        </div>
                                                @endif
                                </div>
                        </div>

                <div class=" mt-5">
                        <div class="container carding-box " id="app">
                                
                                <div class="card box-level" id="test">

                                        <div  class="row mt-2 ">

                                                <div class="col-md-4">
                                                        <h2 class="text-right"></h2>
                                                        <p class="text-right"></p>
                                                </div>

                                                <div class="col-md-4 mt-4">
                                                        <h2 class="text-center">AMG SOFT</h2>
                                                        <p class="text-center">Modules</p>
                                                </div>
                                                <div class="col-md-4">

                                                        <br>
                                                                                <div class="text-center">
                                                                                                <a  href="{{ url('/my-profile') }}" class="btn btn-primary btn-sm pull-left">
                                                                                                        <i class="fas fa-user white"></i> {{ __('My profile') }}
                                                                                                </a>
                                                                                                
                                                                                                <a class="pull-right btn btn-primary btn-sm" href="{{ route('logout') }}"
                                                                                                        onclick="event.preventDefault();
                                                                                                                document.getElementById('logout-form').submit();">
                                                                                                <i class="fas fa-power-off"></i>  {{ __('Logout') }}
                                                                                                </a>
                                                                                </div>
                                                
                                                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                                        @csrf
                                                                                </form>

                                                                        <p class="text-center">Welcome, {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                                                                        <p class="text-center">{{ date('F j, Y, g:i a') }}</p>
                                                                
                                                                     
                                                </div>
                                        </div>

                                        <div class="row"></div>

                                        <div class="row mt-2 mb-5 center-block" style=" justify-content: center;">

                                                        <div class="col-md-3 btn btn-primary m-2 btn-lg boton">
                                                                <a href="{{ route('users') }}">
                                                                        <i class="menu-icon fa fa-users fa-4x white"></i> 
                                                                        <p class="text-uppercase">Users</p>
                                                                </a>
                                                                
                                                        </div>

                                                        <div class="col-md-3 btn btn-primary m-2 btn-lg boton">
                                                                <a href="{{ route('crm') }}">
                                                                        <i class="menu-icon fa fa-th fa-4x white" ></i>
                                                                        <p class="text-uppercase">Crm</p>
                                                                </a>
                                                               
                                                        </div>

                                                        <div class="col-md-3 btn btn-primary m-2 btn-lg boton">
                                                                <a href="{{ route('users') }}">
                                                                        <i class="menu-icon fa fa-building fa-4x white"></i>
                                                                        <p class="text-uppercase">Business</p>
                                                                </a>

                                                               
                                                        </div>

                                                        <div class="col-md-3 btn btn-primary m-2  btn-lg boton">
                                                                <a href="{{ route('users') }}">
                                                                        <i class="menu-icon fa fa-window-maximize fa-4x white"></i>
                                                                        <p class="text-uppercase">Administration</p>
                                                                </a>

                                                              
                                                        </div>
                                                        <div class="col-md-3 btn btn-primary m-2  btn-lg boton">
                                                                <a href="{{ route('users') }}">
                                                                        <i class="menu-icon fa fa-cogs fa-4x white"></i>
                                                                        <p class="text-uppercase">Dept Solution</p>
                                                                </a>

                                                                
                                                        </div>

                                                        <div class="col-md-3 btn btn-primary m-2  btn-lg boton">
                                                                <a href="{{ route('users') }}">
                                                                        <i class="menu-icon fa fa-cloud fa-4x white"></i>
                                                                        <p class="text-uppercase">Cloud</p>
                                                                </a>

                                                               
                                                        </div>
                                        </div>

                                </div> 
                        </div>

                </div>
        @auth
                <script>
                          window.user = @json(auth()->user())

                        
                </script>
        @endauth
  </body>
</html>