@extends('layouts.login_base')

@section('content')

<div class="card box-level mt-4">
        
    <div class="m-3 ">
                <div class="login-box-msg">
                    <h2>{{ __('Member Login') }}</h2>
                </div>
                  <div class="row">
                      
                      <div class="col-md-6">
                           <p class="text-center"> 
                           <img src="{{ asset('images/amgsoft.png') }}" alt="" class="img-fluid logo">
                           </p>
                      </div>
                      <div class="col-md-6">
                            <form method="POST" action="{{ route('login') }}">
                                    @csrf
            
                                    <div class="input-group mb-3">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">
                                            <div class="input-group-append">
                                                    <span class="fa fa-envelope input-group-text"></span>
                                            </div>
                                           
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                    </div>
            
                                    <div class="input-group mb-3">
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                                            <div class="input-group-append">
                                                    <span class="fa fa-lock input-group-text"></span>
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                    </div>
            
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="checkbox icheck">
                                                <label>
                                                    {{ __('Remember Me') }} <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                </label>
                                            </div>
                                        </div>

                                       
                                         

                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary btn-block">
                                                {{ __('Login') }}
                                            </button>
                                        </div>

                                        <div class="col-md-12 mt-2 forgot">
                                                
                                            @if (Route::has('password.request'))
                                                <a style="color: #423f05;" class="" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                    
                              </div>
                                    </div>
                                </form>
                      </div>
                      <div class="col-md-6">

                      </div>
                     
                  </div>

                  

                  

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
    </div>
</div>
@endsection
