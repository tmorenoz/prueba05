@extends('layouts.login_base')

@section('content')
<div class="card box-level">
    <div class="card-body login-card-body">
        
                 <div class="login-box-msg">{{ __('Reset Password') }}</div>
                
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                         <div class="input-group mb-3">
                            
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="Email Address">
                                <div class="input-group-append">
                                        <span class="fa fa-envelope input-group-text"></span>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        

                         <div class="input-group mb-3">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                                <div class="input-group-append">
                                        <span class="fa fa-lock input-group-text"></span>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="input-group mb-3">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Password Confirmation">
                                <div class="input-group-append">
                                        <span class="fa fa-lock input-group-text"></span>
                                </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary btn-block ">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
           
        
    </div>
</div>
@endsection
