@extends('layouts.login_base')

@section('content')
<div class="card box-level">
    <div class="card-body login-card-body">
            <div class="login-box-msg">{{ __('Reset Password') }}</div>
          
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="input-group mb-3">
                            
                            <input id="email" placeholder="Email Address" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                            <div class="input-group-append">
                                    <span class="fa fa-envelope input-group-text"></span>
                            </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary btn-block ">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                            <div class="col-md-4">
                                    <a href="{{ route('login') }}" class="btn btn-primary btn-block ">
                                    {{ __('Login') }}
                                </a>
                            </div>
                        </div>
                    </form>
               
         
        
    </div>
</div>
@endsection
