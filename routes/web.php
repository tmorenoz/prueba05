<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
});



Auth::routes();

Route::get('/register', function () {
    return redirect('login');
})->name('register');

Route::group(['middleware' => 'auth'], function () use ($router) {
    Route::get('/', function () {
        return view('welcome');
    })->middleware('ipRestrict');

    // What's ipRestrict?
    // in file app\Http\Kernel.php in line 66 is middleware protected for ip access
    // in file app\Http\Middleware\ipRestrictMiddleware.php


    Route::get('/home', 'HomeController@panel')->name('home')->middleware('ipRestrict');
    Route::get('/users', 'HomeController@users')->name('users')->middleware('ipRestrict');
    Route::get('/my-profile', 'UserController@profile')->middleware('ipRestrict');
    Route::put('/users/update/{id}','UserController@updateProfile')->name('users.update')->middleware('ipRestrict');

    Route::get('findUser', 'UserController@search');

    Route::get('/profile/{id}','RoleController@show');
    Route::post('/profile/store','RoleController@store');

    // crm 
    Route::get('/crm/leads', 'HomeController@crm')->name('crm')->middleware('ipRestrict');
    // leads 
    Route::post('/crm/leads/deleteall','LeadController@deleteall');
    Route::get('/crm/findlead', 'LeadController@search');
    Route::resource('leads','LeadController');
    
    // lead source, program, users owner lead 
    Route::get('/getdata','LeadController@getData');
  
    // crm leads tasks
    Route::resource('crm/crm-tasks', 'TaskController');
      // crm leads events
    Route::resource('crm/crm-events', 'EventController');
       // crm leads notes
    Route::resource('crm/crm-notes', 'NoteController');

    // crm leads tasks and priority
    Route::get('/getdata/ext','TaskController@getData');

    // general routes for VUE ROUTER in resources/views/js/app.js

    Route::get('{path}','HomeController@index')->where('path', '([A-z\d-\/_.]+)?')->middleware('ipRestrict');
    
});