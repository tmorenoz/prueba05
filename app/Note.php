<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes; 

use App\User;
use App\Lead;


class Note extends Model
{
    use SoftDeletes;

    protected $table = 'notes';

    protected $fillable = [
        'user_id',
        'lead_id',
        'text'
    ];

    protected $dates = ['deleted_at']; 

    public function users(){
            return $this->belongsTo(User::class, 'user_id');
    }

    public function lead(){
        return $this->belongsTo(Lead::class, 'lead_id');
    }
}
