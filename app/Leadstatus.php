<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leadstatus extends Model
{
    protected $table = 'leadstatuses';

    protected $fillable = [
        'value'
    ];
    
}
