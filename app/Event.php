<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lead;
use App\User;
use App\Participant;

use Illuminate\Database\Eloquent\SoftDeletes; 
class Event extends Model
{
    use SoftDeletes; // mode softdelete

    protected $table = 'events';

    protected $fillable = [
        'title',
        'location',
        'all_day',
        'from',
        'to',
        'user_id',
        'lead_id',
        'description'
    ];

    protected $dates = ['deleted_at']; 

    public function lead(){
        return $this->belongsTo(Lead::class);
    }

    public function users(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function participants(){
        return $this->hasMany(Participant::class);
    }

}
