<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calendar;
use App\User;
use App\Event;
use Carbon\Carbon;
use DB;
use Auth;
use function GuzzleHttp\json_encode;


class CalendarController extends Controller
{
    public function index()
    {

        $users = User::find(Auth::id());
        
        //dd($user);

        $calendars = DB::table('events')
        ->join('users', 'events.user_id', '=', 'users.id')
        ->join('leads', 'events.lead_id', '=', 'leads.id')
        ->join('users as us','events.created_users','=','us.id')
        ->join('users as usr','events.update_users','=','usr.id')
        ->select(DB::raw('events.id,events.title,events.from as start,events.description,events.location,events.user_id,events.lead_id,DATE_FORMAT(events.from,"%W, %b %e") as current_day,DATE_FORMAT(events.from , "%H %i %p") as date_ini,DATE_FORMAT(events.to , "%H %i %p") as date_fin,users.image,users.first_name as h_firstname,users.last_name as h_lastname,leads.first_name as l_firstname,leads.last_name as l_last_name,events.created_users,DATE_FORMAT(events.created_at,"%a, %d %b %Y %r") as created_at,events.update_users,
        DATE_FORMAT(events.updated_at,"%a, %d %b %Y %r") as updated_at,CONCAT(us.first_name," ",us.last_name) AS created_users, 
        CONCAT(usr.first_name," ",usr.last_name) AS update_users'))
        ->get();

        //dd($calendars);
        return $calendars;
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //dd($id);
       
        $users = User::find(Auth::id());

        //$user=$users->id;

        $date = Carbon::parse($request->from, 'UTC');
        $from=$date->toDateTimeString();

        $date = Carbon::parse($request->to, 'UTC');
        $to=$date->toDateTimeString();

        $this->validate($request, [
            'user_id' => 'required',
            'title' => 'required|string|max:191',
        ]);

        $event = Event::findOrFail($id);
        
        $event->user_id = $request['user_id'];
        $event->title = $request['title'];
        $event->location = $request['location'];
        $event->from = $from;
        $event->to = $to;
        $event->description = $request['description'];
        $event->lead_id = $request['lead_id'];
        $event->update_users =$users->id;//tmz-04/03/2019
        $event->save();
        
        return $event;
    }

    public function destroy($id)
    {
        //
    }


}
