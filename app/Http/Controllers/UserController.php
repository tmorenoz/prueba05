<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            // list data users with relationship roles and module
            return User::with(['roles','module'])->latest()->paginate(5);
           
      
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function profile()
    { 
        // profile user get is in blade file
        $user = User::find(Auth::id());
       
        return view('profile.index', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $this->validate($request, [
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'cellphone' => $request['cellphone'],
            'address' => $request['address'],
            'password' => bcrypt($request['password']),
            'date_of_birth' => $request['date_of_birth'],
            'restriction_by_ip' => $request['restriction_by_ip'],
            'status' => $request['status'],
        ]);

            //$user->assignRole($request->get('role'));

          if(count($request->get('module_role')) > 0)
          {

                for($x=0;$x<count($request->get('module_role')); $x++)
                {
                       
                         DB::table('role_user')->insert(
                                [
                                    'user_id' => $user->id,
                                    'role_id' => $request->get('module_role')[$x]['namerol'],
                                    'module_id' => $request->get('module_role')[$x]['module']
                                ]
                            );
                }
               
          }


        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // find user or update information
        $user = User::findOrFail($id);
        

        $this->validate($request,[
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password' => 'sometimes|min:6'
        ]);
       
        
            $user->first_name = $request['first_name'];
            $user->last_name = $request['last_name'];
            $user->email = $request['email'];
            $user->phone = $request['phone'];
            $user->cellphone = $request['cellphone'];
            $user->address = $request['address'];
            $user->restriction_by_ip = $request['restriction_by_ip'];
            $user->status = $request['status'];
        
            if(!$request['password'])
            {
                $user->password = bcrypt($request['password']);
            }
            
            $user->date_of_birth = $request['date_of_birth'];
            $user->save();
            //after save data if request module_role exist then is process create permission and role in USER

            if(count($request->get('module_role')) > 0)
            {
                $userRole = DB::table('role_user')->where('user_id', $user->id)->get();

                    if(count($userRole) > 0){
                        DB::table('role_user')->where('user_id', $user->id)->delete();
                    }
                
                  for($x=0;$x<count($request->get('module_role')); $x++)
                  {
                         
                           DB::table('role_user')->insert(
                                  [
                                      'user_id' => $user->id,
                                      'role_id' => $request->get('module_role')[$x]['namerol'],
                                      'module_id' => $request->get('module_role')[$x]['module']
                                  ]
                              );
                  }
                 
            }


        return ['message' => 'Updated the user info'];
    }

    public function updateProfile(Request $request, $id)
    {
        $user = User::findOrFail($id);
      
        
        $this->validate($request,[
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191'
        ]);
    
            $user->first_name = $request['first_name'];
            $user->last_name = $request['last_name'];
            $user->phone = $request['phone'];
            $user->cellphone = $request['cellphone'];
            $user->address = $request['address'];
           
            if(!$request['password'])
            {
                $user->password = bcrypt($request['password']);
            }
            
            $user->date_of_birth = $request['date_of_birth'];
            $user->save();

        return redirect('/home')->with('status','Updated the user info');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        // delete the user
        $user->delete();
        return ['message' => 'User Deleted'];
    }

    public function search(){

        if ($search = \Request::get('q')) {

            if($search == ''){
                $users = User::with(['roles','module'])->latest()->paginate(5);
            }else{
                $users = User::with(['roles','module'])
                                ->rol($search) // process in app\User.php is in scopeRol
                                        ->paginate(20);
            }

        }else{
            $users = User::with(['roles','module'])->latest()->paginate(5);
        }
        return $users;
    }

 
}
