<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // apply process validate before create event

        $this->validate($request, [
            'user_id' => 'required',
            'title' => 'required|string|max:191',
            'lead_id' => 'required',

        ]);

        $events = Event::create([
            'user_id' => $request['user_id'],
            'title' => $request['title'],
            'location' => $request['location'],
            'all_day' => $request['all_day'],
            'from' => $request['from'],
            'to' => $request['to'],
            'description' => $request['description'],
            'lead_id' => $request['lead_id'],
            
        ]);

        $arrayParticipanting = $request->get('participants');

        if( $arrayParticipanting ){
            // apply process for insert participants in array

                for( $x=0; $x<count($arrayParticipanting);$x++){
                    DB::table('participants')->insert([
                        'event_id' => $events->id,
                        'user_id'  => $arrayParticipanting[$x],
                    ]);
                }
           
        }

        return $events;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Event::with(['participants.users','users','lead'])->where('lead_id', $id)->paginate(3);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'title' => 'required|string|max:191',
            'lead_id' => 'required',

        ]);

        $event = Event::findOrFail($id);

        $event->user_id = $request['user_id'];
        $event->title = $request['title'];
        $event->location = $request['location'];
        $event->all_day = $request['all_day'];
        $event->from = $request['from'];
        $event->to = $request['to'];
        $event->description = $request['description'];
        $event->lead_id = $request['lead_id'];
        $event->save();
        
        return $event;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        if($event) {
            $event->delete();
        }

        return ('Event delete');
    }
}
