<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleP extends Model
{
     /**
     * The attributes that are fillable via mass assignment.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'special'];

    protected $primarykey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The shinobi cache tag used by the model.
     *
     * @return string
     */

     public function module()
     {
         return $this->hasMany(Module::class);
     }
}
