<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes; 

class Lead extends Model
{
    use  SoftDeletes;

    protected $table ='leads'; // mode softdelete

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'source_id',
        'leadsourcenames',
        'program_id',
        'phone',
        'mobile',
        'credit_report',
        'payment20',
        'payment50',
        'leadstatus_id',
        'street',
        'city',
        'state',
        'country',
        'zipcode',
        'description',
        'created_by'
    ];

    protected $dates = ['deleted_at']; 

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function status()
    {
        return $this->belongsTo(Leadstatus::class,'leadstatus_id');
    }

    public function createby()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function source()
    {
        return $this->belongsTo(Leadsource::class,'source_id');
    }

    public function program()
    {
        return $this->belongsTo(Program::class,'program_id');
    }

    // scopes for search data filter

        public function scopeLead($query, $search){
            if($search)
                return $query->where('first_name','LIKE',"%$search%")
                                ->orWhere('last_name','LIKE',"%$search%")
                                    ->orWhere('mobile','LIKE',"%$search%")
                                        ->orWhereHas('status', function($s) use ($search) {
                                            $s->where('value','LIKE',"%$search%");
                                        })
                                        ->orWhereHas('user', function($s) use ($search) {
                                            $s->where('first_name','LIKE',"%$search%");
                                        });
        }

       
}
