
## About AMGSOFT

### Modules
1. Users (Ready)
2. Crm (In process)

### Branches
1. master
2. release
3. Feature/T1-login
4. Feature/T2-users
5. Feature/T3-profile
6. Feature/T4-crm-leads
7. Feature/T5-styles
8. Hotfix
9. develop


### Initial Project
1. composer install
2. php artisan migrate --seed
3. php artisan serve
4. join 

### Modified Project
1. composer install
2. npm install
3. modified file .env
4. database configure crm_db
5. php artisan migrate --seed
6. php artisan serve
7. modified files any .vue you run script terminal npm run dev for precompiled js
8. modified files css in resources/views/sass/app.scss same run npm run dev

### About views
Routes in resources/views/js/app.js with vue router, every module is component of vue.

### Packages externals use
1. animate.css
2. sweetalert2
3. vue-router
4. vue-select
5. laravel-vue-pagination
6. caffeinated/shinobi
